﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using XBot.Overlay;

namespace XBot
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            if (WindowState == FormWindowState.Normal)
            {
                this.Show();
                this.ShowInTaskbar = true;
            }
            if (WindowState == FormWindowState.Minimized)
            {
                this.Hide();
                this.ShowInTaskbar = false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            notifyIcon1.ContextMenuStrip = contextRightClickmenu;

            comboBoxGameVersions.Items.Clear();
            //find active processes
            foreach (Process x in Process.GetProcesses()
                .Where(p => p.ProcessName.ToLower().StartsWith("xsyonsteam")
                || p.ProcessName.ToLower() == "xsyon"))
            {
                XsyonClient client = new XsyonClient(x);
                comboBoxGameVersions.Items.Add(client);
            }
            if (comboBoxGameVersions.Items.Count > 0)
            comboBoxGameVersions.SelectedIndex = 0;
        }

        private void showToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            WindowState = FormWindowState.Normal;
            
            this.ShowInTaskbar = true;
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_Launch_Click(object sender, EventArgs e)
        {
            if (comboBoxGameVersions.Items.Count == 0)
                return;

            XsyonData data = comboBoxGameVersions.SelectedItem as XsyonData;

            // data.TheProcess is our canidate
            OverlayForm of = new OverlayForm(data.GetGameProcess());
            of.Show();

            this.Hide();
            this.ShowInTaskbar = false;
        }
    }
}
