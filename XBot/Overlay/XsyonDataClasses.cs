﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XBot.Overlay
{
    internal interface XsyonData
    {
        Process GetGameProcess();
        void SetGameProcess(Process value);
        string GetExePath();
    }
    public class XsyonClient : XsyonData
    {
        public Process TheProcess;

        public XsyonClient(Process theProcess)
        {
            TheProcess = theProcess;
        }
        public string GetExePath()
        {
            return TheProcess.MainModule.FileName;
        }

        public Process GetGameProcess()
        {
            return TheProcess;
        }

        public void SetGameProcess(Process value)
        {
            TheProcess = value;
        }

        public override string ToString()
        {
            return string.Format("{0}:{1}", TheProcess.ProcessName, TheProcess.Id);
        }
    }
}
