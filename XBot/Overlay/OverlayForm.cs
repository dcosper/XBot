﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static XBot.USER32;

namespace XBot.Overlay
{
    public partial class OverlayForm : Form
    {
        Color _backGround = Color.Magenta; // crap background noone uses
        RECT _rect; // the rect of the process window
        WinEventDelegate _windowDelegate;
        public Process TheProcess; // the process we are controlling
        Timer moveTimer;
        Timer useTimer;
        public OverlayForm(Process theProcess)
        {
            InitializeComponent();
            TheProcess = theProcess;
            _windowDelegate = new WinEventDelegate(WindowMoveCallback);
        }
        
        private void OverlayForm_Load(object sender, EventArgs e)
        {
            this.BackColor = _backGround;
            this.TransparencyKey = _backGround;
            this.TopMost = true;
            this.FormBorderStyle = FormBorderStyle.None;
            //int style = USER32.GetWindowLong(this.Handle, -20);
            //USER32.SetWindowLong(this.Handle, -20, style | 0x80000 | 0x20);
           // USER32.SetWindowLong(button1.Handle, 0, style);
            SetLocation();

            // Set the hook so we know when window is moved
            
            USER32.SetWinEventHook(USER32.EVENT_SYSTEM_MOVESIZESTART,
                USER32.EVENT_SYSTEM_MOVESIZEEND,
                IntPtr.Zero,
                _windowDelegate,
                (uint)TheProcess.Id,
                (uint)0,
                USER32.WINEVENT_OUTOFCONTEXT);

            foreach(Keys key in Enum.GetValues(typeof(Keys)))
            {
                comboBoxKeys1.Items.Add(key);
                comboBoxKeys2.Items.Add(key);
            }
            comboBoxKeys1.SelectedIndex = 0;
            comboBoxKeys2.SelectedIndex = 0;
            groupBox1.Hide();
        }
        private void SetLocation()
        {
            USER32.GetWindowRect(TheProcess.MainWindowHandle, out _rect);

            this.Size = new Size(_rect.Width, _rect.Height);
            this.Top = _rect.Top;
            this.Left = _rect.Left;
        }
        private void WindowMoveCallback(IntPtr hWinEventHook, uint eventType, IntPtr hwnd, int idObject, int idChild, uint dwEventThread, uint dwmsEventTime)
        {
            SetLocation();
        }

        private void OverlayForm_Paint(object sender, PaintEventArgs e)
        {
            // update the rect we are controlling
            USER32.GetWindowRect(TheProcess.MainWindowHandle, out _rect);

            // Use Graphics to draw to overlay here

            using (Graphics g = e.Graphics)
            {
               // g.DrawRectangle(new Pen(Color.Red), _rect.Width - 205,
                  //  _rect.Bottom - 105,
                  //  35, 35);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(btn_Hide.Text == ">")
            {
                groupBox1.Show();
                btn_Hide.Text = "<";
            }
            else
            {
                groupBox1.Hide();
                btn_Hide.Text = ">";
            }
        }

        private void btn_UseKey_Click(object sender, EventArgs e)
        {
            if (btn_UseKey.Text == "Start")
            {
                useTimer = new Timer();
                useTimer.Interval = 8000;
                useTimer.Tick += UseTimerTick;
                useTimer.Start();
                btn_UseKey.Text = "Stop";
                UseTimerTick(null, null);
            }
            else
            {
                if(useTimer != null)
                {
                    useTimer.Tick -= UseTimerTick;
                    useTimer.Stop();
                }
                btn_UseKey.Text = "Start";
            }
            
        }

       

        private void btn_Move_Click(object sender, EventArgs e)
        {
            if (btn_Move.Text == "Move")
            {
                moveTimer = new Timer();
                moveTimer.Interval = 8000;
                moveTimer.Tick += MoveTimerTick;
                moveTimer.Start();
                btn_Move.Text = "Stop";
                MoveTimerTick(null, null);
            }
            else
            {
                if (moveTimer != null)
                {
                    moveTimer.Tick -= MoveTimerTick;
                    moveTimer.Stop();
                }
                btn_Move.Text = "Move";
            }
        }
        private async void UseTimerTick(object sender, EventArgs e)
        {
            Keys key = (Keys)comboBoxKeys2.SelectedItem;
            if (string.IsNullOrEmpty(key.ToString()))
                return;
            await PostMessageTools.SendKey(TheProcess.MainWindowHandle, key);
        }
        private async void MoveTimerTick(object sender, EventArgs e)
        {
            Keys key = (Keys)comboBoxKeys1.SelectedItem;
            if (string.IsNullOrEmpty(key.ToString()))
                return;
            await PostMessageTools.MoveAndPerformKey(TheProcess.MainWindowHandle, 25, key);
        }
    }
}
