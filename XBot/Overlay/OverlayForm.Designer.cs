﻿namespace XBot.Overlay
{
    partial class OverlayForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Hide = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_Move = new System.Windows.Forms.Button();
            this.comboBoxKeys1 = new System.Windows.Forms.ComboBox();
            this.btn_UseKey = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxKeys2 = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Hide
            // 
            this.btn_Hide.Location = new System.Drawing.Point(3, 0);
            this.btn_Hide.Name = "btn_Hide";
            this.btn_Hide.Size = new System.Drawing.Size(23, 23);
            this.btn_Hide.TabIndex = 0;
            this.btn_Hide.Text = ">";
            this.btn_Hide.UseVisualStyleBackColor = true;
            this.btn_Hide.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxKeys2);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btn_UseKey);
            this.groupBox1.Controls.Add(this.comboBoxKeys1);
            this.groupBox1.Controls.Add(this.btn_Move);
            this.groupBox1.Location = new System.Drawing.Point(32, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(330, 124);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // btn_Move
            // 
            this.btn_Move.Location = new System.Drawing.Point(239, 24);
            this.btn_Move.Name = "btn_Move";
            this.btn_Move.Size = new System.Drawing.Size(75, 23);
            this.btn_Move.TabIndex = 0;
            this.btn_Move.Text = "Move";
            this.btn_Move.UseVisualStyleBackColor = true;
            this.btn_Move.Click += new System.EventHandler(this.btn_Move_Click);
            // 
            // comboBoxKeys1
            // 
            this.comboBoxKeys1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxKeys1.FormattingEnabled = true;
            this.comboBoxKeys1.Location = new System.Drawing.Point(144, 24);
            this.comboBoxKeys1.Name = "comboBoxKeys1";
            this.comboBoxKeys1.Size = new System.Drawing.Size(74, 24);
            this.comboBoxKeys1.TabIndex = 1;
            // 
            // btn_UseKey
            // 
            this.btn_UseKey.Location = new System.Drawing.Point(239, 55);
            this.btn_UseKey.Name = "btn_UseKey";
            this.btn_UseKey.Size = new System.Drawing.Size(75, 23);
            this.btn_UseKey.TabIndex = 3;
            this.btn_UseKey.Text = "Start";
            this.btn_UseKey.UseVisualStyleBackColor = true;
            this.btn_UseKey.Click += new System.EventHandler(this.btn_UseKey_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 17);
            this.label1.TabIndex = 4;
            this.label1.Text = "Move then Use Key:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(61, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Use Key:";
            // 
            // comboBoxKeys2
            // 
            this.comboBoxKeys2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxKeys2.FormattingEnabled = true;
            this.comboBoxKeys2.Location = new System.Drawing.Point(144, 54);
            this.comboBoxKeys2.Name = "comboBoxKeys2";
            this.comboBoxKeys2.Size = new System.Drawing.Size(74, 24);
            this.comboBoxKeys2.TabIndex = 6;
            // 
            // OverlayForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 467);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Hide);
            this.Name = "OverlayForm";
            this.Text = "OverlayForm";
            this.Load += new System.EventHandler(this.OverlayForm_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.OverlayForm_Paint);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Hide;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_Move;
        private System.Windows.Forms.ComboBox comboBoxKeys2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_UseKey;
        private System.Windows.Forms.ComboBox comboBoxKeys1;
    }
}