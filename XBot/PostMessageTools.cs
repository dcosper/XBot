﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XBot
{
    public static class PostMessageTools
    {
        public static async Task<bool> SendKey(IntPtr window, Keys key)
        {
            var tcs = new TaskCompletionSource<bool>();
            USER32.PostMessage(window, USER32.WM_KEYDOWN, (IntPtr)key, (UIntPtr)0x00030001);
            await Task.Delay(100);
            USER32.PostMessage(window, USER32.WM_KEYUP, (IntPtr)key, (UIntPtr)0xC0030001);
            tcs.TrySetResult(true);
            return await tcs.Task;
        }
        public static void SendRightMouseClick(IntPtr window, int x, int y)
        {
            USER32.PostMessage(window, USER32.WM_RBUTTONDOWN, (IntPtr)0x00000002, (UIntPtr)MakeLParam(x, y));
            Thread.Sleep(100);
            USER32.PostMessage(window, USER32.WM_RBUTTONUP, (IntPtr)0x00000000, (UIntPtr)MakeLParam(x, y));
        }
        public static async Task<bool> MoveAndPerformKey(IntPtr window, int spaces, Keys useKey)
        {
            var tcs = new TaskCompletionSource<bool>();

            for (int i = 1; i < spaces; i++)
                await SendKey(window, Keys.W); // forward
            
            await SendKey(window, useKey);

            tcs.TrySetResult(true);
            return await tcs.Task;
        }
        public static int MakeLParam(int loword, int hiword)
        {
            return ((hiword << 16) | (loword & 0xffff));
        }
    }
}
